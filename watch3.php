<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>star_jewelry</title>
  <link rel="stylesheet" type="text/css" href="css/base.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/watch1.css">
</head>

<body>

  <?php
   require "base.php";
  ?>
<form action="cart.php" method="POST">
 <div id="page_box">
 <div id="page1">
  <div class="page_img">
   <a href="#"><img src="img/2SW1028_As.jpg" alt="時計3"></a>
  </div>
  <div class="page_comment">

      <input type="hidden" name="img" value="img/2SW1028_As.jpg">
      <input type="hidden" name="title" value="COSMIC DAY&NIGHT(3)">
      <input type="hidden" name="productid" value="品番：2SW1028">
      <input type="hidden" name="quantity" value="1">
      <input type="hidden" name="price" value="￥29,160 (tax in)">

      <div id="title_pop">
       <a>COSMIC DAY&NIGHT<br>￥29,160 (tax in)</a>
      </div>

    <div class="message">
    <a>【2019 Summer Limited】<br>
      夏の夜空を表現した限定ウォッチ。<br>
      24時間のうち、太陽と月が交互に顔を出し、<br>
      時を告げる幻想的なデザイン。<br>
      ジュエリー感覚でコーディネイトを楽しめるブレスレットタイプです。<br>
      【数量限定・スペシャルパッケージ付】<br>
      素材 ：STEEL<br>
      詳細 ：クリスタルガラス<br>
      ムーブメント：クォーツ・DAY & NIGHT表示機能付き<br>
      日常生活用防水(3気圧) <br>

  </div>
  <div id="button">
   <input type="submit" title="ADD_TO_BAG" value="ADD TO BAG" id ="ADD_TO_BAG">
  </div>
 </div>
 </div>

 <div id=footer>
  <a>© STAR JEWELRY CO.,LTD</a>
 </div>

</body>
</html>
