<!-- MｙSQL
PDO接続
CRUD処理（クリエイト作る、リード参照、アップデート編集、デリート削除）
  -->
 <?php
// require_once("../config/config.php");
// require_once("../model/Cart.php");
//
// try{
//   $user = new User($host, $dbname, $user, $pass);
//   $user->connectDb();
//
//
//  //削除処理
//  if(isset($_GET['del'])) {
//    $user->delete($_GET['del']);
//  }
//    if(isset($_GET['edit'])) {
//
//    //編集処理
//    if($_POST){
//     $user->edit($_POST);
//      }
//
//    //参照処理
//    $result['User'] = $user->findById($_GET['edit']);
//   }
//  else {
//    //登録処理
//    if($_POST){
//     $user->add($_POST);
//     }
//     //参照処理
//     $result = $user->findALL();
//    }
//   }
//
// catch (PDOException $e) { //PDOExcetionをキャッチする
//   echo "エラー!!:" . $e->getMessage();
// }
?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>star_jewelry</title>
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/base.css">
  <!-- <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/index.js"></script> -->
</head>

<body>

  <?php
   require "base.php";
  ?>

<div id="content">

   <div id="plus_menu">
     <ul>
       <li>
        <a href="#">リング</a>
       </li>
       <li>
        <a href="#">ネックレス</a>
       </li>
       <li>
        <a href="#">ピアス</a>
       </li>
       <li>
        <a href="#">ブレスレッド</a>
       </li>
       <li>
        <a href="#">ウォッチ</a>
       </li>
       <li>
        <a href="#">カップル</a>
       </li>
       <li>
        <a href="#">ベビー</a>
       </li>
       <li>
        <a href="#">その他</a>
       </li>
       <li>
        <a href="#">ALL</a>
       </li>

     </ul>
   </div>

 <div id="list_section">
   <ul>

 <!-- 1枚目 -->
    <li>
    <div class="product_img">
     <a href="watch1.php"><img src="img/2SW1026_As.jpg" alt="時計１"></a>
    </div>
    <div class="product_title">
      <a>STEEL<br>ウォッチ<br>限定商品<br>SUMMER RAIN</a>
    </div>
    <div class="product_price">
      <a>￥27,000</a>
    </div>
    </li>

<!-- ２枚目 -->
    <li>
    <div class="product_img">
     <a href="watch2.php"><img src="img/2SW1025_As.jpg" alt="時計２．"></a>
    </div>
    <div class="product_title">
      <a>STEEL<br>ウォッチ<br>限定商品<br>SUMMER RAIN</a>
    </div>
    <div class="product_price">
      <a>￥27,000</a>
    </div>
    </li>

<!-- ３枚目 -->
    <li>
    <div class="product_img">
     <a href="watch3.php"><img src="img/2SW1028_As.jpg" alt="時計３"></a>
    </div>
    <div class="product_title">
      <a>STEEL<br>ウォッチ<br>限定商品<br>COSMIC DAY&NIGHT</a>
    </div>
    <div class="product_price">
      <a>￥29,160</a>
    </div>
    </li>

<!-- 4枚目 -->
    <li>
    <div class="product_img">
     <a href="watch4.php"><img src="img/2SW1027_As.jpg" alt="時計４"></a>
    </div>
    <div class="product_title">
      <a>STEEL<br>ウォッチ<br>限定商品<br>COSMIC DAY&NIGHT</a>
    </div>
    <div class="product_price">
      <a>￥29,160</a>
    </div>
    </li>

   </ul>

 </div>

 </div>

 <div id=footer>
  <a>© STAR JEWELRY CO.,LTD</a>
 </div>

</body>
</html>
