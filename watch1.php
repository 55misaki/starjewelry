<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>star_jewelry</title>
  <link rel="stylesheet" type="text/css" href="css/base.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/watch1.css">
</head>

<body>

  <?php
   require "base.php";
  ?>

 <form action="cart.php" method="POST">

  <div id="page_box">
  <div id="page1">
   <div class="page_img">
    <a href="#"><img src="img/2SW1026_As.jpg" alt="時計１"></a>
   </div>
  <div class="page_comment">

      <input type="hidden" name="img" value="img/2SW1026_As.jpg">
      <input type="hidden" name="title" value="SUMMER RAIN(1)">
      <input type="hidden" name="productid" value="品番：2SW1026">
      <input type="hidden" name="quantity" value="1">
      <input type="hidden" name="price" value="￥27,000 (tax in)">

      <div id="title_pop">
       <a>SUMMER RAIN<br>￥27,000 (tax in)</a>
      </div>

    <div class="message">
    <a>【2019 Summer Limited】<br>
       トランスペアレントな輝きを楽しめる限定ウォッチが数量限定で登場。<br>
       夏の雨をイメージした”プリズム”のモチーフが<br>フェイスにデザインされています。<br>
      【数量限定】<br>
      素材 ：STEEL<br>
      詳細 ：マザーオブパール、クリスタルガラス<br>
      ベルト：本牛革<br>
      ベルト幅:約1cm、ケース厚み約0.7cm、フェイス約3.2cm</a>

  </div>
 </div>
 </div>

 <div id=footer>
  <a>© STAR JEWELRY CO.,LTD</a>
 </div>

</body>
</html>
